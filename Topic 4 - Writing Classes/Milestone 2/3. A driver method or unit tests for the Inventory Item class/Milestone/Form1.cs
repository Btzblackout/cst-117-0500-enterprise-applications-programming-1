﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Milestone
{
    public partial class InventoryManagement : Form
    {
        //Create and populate the inventory item list.
        List<InventoryItem> ItemList = new List<InventoryItem>()
            {
                new InventoryItem ("Candle 1", "This is the description of the first candle.", "Parafin", "Cored", "Blue", "Blueberry", false, 4, 40, 5.00),
                new InventoryItem ("Candle 2", "This is the second candle.", "Bees", "Flat-Braid", "Brown", "Cinnamon", true, 8, 15, 10.00),
                new InventoryItem ("Candle 3", "This is the third candle in the inventory list.", "Soy", "Square-Braid", "White", "Vanila", false, 12, 50, 15.00),
                new InventoryItem ("Candle 4", "This is the fourth candles description.", "Palm", "HTP", "Yellow", "Lemon", false, 4, 2, 5.00)
            };
        //
        bool loggedIn = false;
        bool editingInv = false;
        public InventoryManagement()
        {
            
            InitializeComponent();
        }

        //When this button is clicked it validates the users input and if it meets expectations it continues the program to the menu screen.
        private void LoginButton_Click(object sender, EventArgs e)
        {
            if(UsernameTextBox.Text.ToLower() == "admin" && PasswordTextBox.Text.ToLower() == "admin")
            {
                loggedIn = true;
                MenuPanel.Visible = true;
                UsernameTextBox.Clear();
                PasswordTextBox.Clear();
            }
        }

        //If this button is clicked it logs the user out and activates the login screen.
        private void LogoutButton_Click(object sender, EventArgs e)
        {
            loggedIn = false;
            MenuPanel.Visible = false;
        }

        //This button closes the inventory screen, going back to the main menu screen.
        private void BackButton1_Click(object sender, EventArgs e)
        {
            InventoryPanel.Visible = false;
        }

        //This button is used to go to the inventory screen from the main menu.
        private void InventoryButton_Click(object sender, EventArgs e)
        {
            InventoryPanel.Visible = true;
            inventoryItemBindingSource.Clear();
            
            //Iterates through the item list and adds it to the DataGridView Control
            for (int i = 0; i <= ItemList.Count() - 1; i++)
            {
                inventoryItemBindingSource.Add(ItemList[i]);

            }
            dataGridView1.AutoResizeColumns();
        }

        
    }
}
