﻿namespace Milestone
{
    partial class InventoryManagement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.LoginHeader = new System.Windows.Forms.Label();
            this.UsernameLabel = new System.Windows.Forms.Label();
            this.PasswordLabel = new System.Windows.Forms.Label();
            this.LoginTag = new System.Windows.Forms.Label();
            this.UsernameTextBox = new System.Windows.Forms.TextBox();
            this.PasswordTextBox = new System.Windows.Forms.TextBox();
            this.LoginButton = new System.Windows.Forms.Button();
            this.MenuPanel = new System.Windows.Forms.Panel();
            this.InventoryPanel = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descriptionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.waxTypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wickTypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.scentDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.shimmeringDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.sizeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.quantityDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastOrderedQuantityDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.priceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastOrderedDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.inventoryItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.InventoryTag = new System.Windows.Forms.Label();
            this.InventoryHeader = new System.Windows.Forms.Label();
            this.BackButton1 = new System.Windows.Forms.Button();
            this.MenuHeader = new System.Windows.Forms.Label();
            this.InventoryButton = new System.Windows.Forms.Button();
            this.LogoutButton = new System.Windows.Forms.Button();
            this.MenuTag = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.MenuPanel.SuspendLayout();
            this.InventoryPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inventoryItemBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // LoginHeader
            // 
            this.LoginHeader.AutoSize = true;
            this.LoginHeader.Font = new System.Drawing.Font("Stencil", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LoginHeader.Location = new System.Drawing.Point(88, 9);
            this.LoginHeader.Name = "LoginHeader";
            this.LoginHeader.Size = new System.Drawing.Size(634, 57);
            this.LoginHeader.TabIndex = 0;
            this.LoginHeader.Text = "INVENTORY MANAGEMENT";
            // 
            // UsernameLabel
            // 
            this.UsernameLabel.AutoSize = true;
            this.UsernameLabel.Font = new System.Drawing.Font("Stencil", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UsernameLabel.Location = new System.Drawing.Point(125, 233);
            this.UsernameLabel.Name = "UsernameLabel";
            this.UsernameLabel.Size = new System.Drawing.Size(97, 19);
            this.UsernameLabel.TabIndex = 1;
            this.UsernameLabel.Text = "Username:";
            // 
            // PasswordLabel
            // 
            this.PasswordLabel.AutoSize = true;
            this.PasswordLabel.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.PasswordLabel.Font = new System.Drawing.Font("Stencil", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PasswordLabel.Location = new System.Drawing.Point(125, 291);
            this.PasswordLabel.Name = "PasswordLabel";
            this.PasswordLabel.Size = new System.Drawing.Size(100, 19);
            this.PasswordLabel.TabIndex = 2;
            this.PasswordLabel.Text = "Password: ";
            // 
            // LoginTag
            // 
            this.LoginTag.AutoSize = true;
            this.LoginTag.Font = new System.Drawing.Font("Stencil", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LoginTag.Location = new System.Drawing.Point(330, 66);
            this.LoginTag.Name = "LoginTag";
            this.LoginTag.Size = new System.Drawing.Size(96, 34);
            this.LoginTag.TabIndex = 3;
            this.LoginTag.Text = "LOGIN";
            // 
            // UsernameTextBox
            // 
            this.UsernameTextBox.Location = new System.Drawing.Point(304, 232);
            this.UsernameTextBox.Name = "UsernameTextBox";
            this.UsernameTextBox.Size = new System.Drawing.Size(183, 20);
            this.UsernameTextBox.TabIndex = 4;
            // 
            // PasswordTextBox
            // 
            this.PasswordTextBox.Location = new System.Drawing.Point(304, 290);
            this.PasswordTextBox.Name = "PasswordTextBox";
            this.PasswordTextBox.PasswordChar = '*';
            this.PasswordTextBox.Size = new System.Drawing.Size(183, 20);
            this.PasswordTextBox.TabIndex = 5;
            // 
            // LoginButton
            // 
            this.LoginButton.Font = new System.Drawing.Font("Stencil", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LoginButton.Location = new System.Drawing.Point(326, 386);
            this.LoginButton.Name = "LoginButton";
            this.LoginButton.Size = new System.Drawing.Size(127, 37);
            this.LoginButton.TabIndex = 6;
            this.LoginButton.Text = "Login";
            this.LoginButton.UseVisualStyleBackColor = true;
            this.LoginButton.Click += new System.EventHandler(this.LoginButton_Click);
            // 
            // MenuPanel
            // 
            this.MenuPanel.Controls.Add(this.InventoryPanel);
            this.MenuPanel.Controls.Add(this.MenuHeader);
            this.MenuPanel.Controls.Add(this.InventoryButton);
            this.MenuPanel.Controls.Add(this.LogoutButton);
            this.MenuPanel.Controls.Add(this.MenuTag);
            this.MenuPanel.Location = new System.Drawing.Point(12, 12);
            this.MenuPanel.Name = "MenuPanel";
            this.MenuPanel.Size = new System.Drawing.Size(776, 429);
            this.MenuPanel.TabIndex = 7;
            this.MenuPanel.Visible = false;
            // 
            // InventoryPanel
            // 
            this.InventoryPanel.Controls.Add(this.dataGridView1);
            this.InventoryPanel.Controls.Add(this.InventoryTag);
            this.InventoryPanel.Controls.Add(this.InventoryHeader);
            this.InventoryPanel.Controls.Add(this.BackButton1);
            this.InventoryPanel.Location = new System.Drawing.Point(1, 0);
            this.InventoryPanel.Name = "InventoryPanel";
            this.InventoryPanel.Size = new System.Drawing.Size(776, 429);
            this.InventoryPanel.TabIndex = 4;
            this.InventoryPanel.Visible = false;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameDataGridViewTextBoxColumn,
            this.descriptionDataGridViewTextBoxColumn,
            this.waxTypeDataGridViewTextBoxColumn,
            this.wickTypeDataGridViewTextBoxColumn,
            this.colorDataGridViewTextBoxColumn,
            this.scentDataGridViewTextBoxColumn,
            this.shimmeringDataGridViewCheckBoxColumn,
            this.sizeDataGridViewTextBoxColumn,
            this.quantityDataGridViewTextBoxColumn,
            this.lastOrderedQuantityDataGridViewTextBoxColumn,
            this.priceDataGridViewTextBoxColumn,
            this.lastOrderedDateDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.inventoryItemBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(10, 205);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(762, 206);
            this.dataGridView1.TabIndex = 3;
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "Name";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            this.nameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // descriptionDataGridViewTextBoxColumn
            // 
            this.descriptionDataGridViewTextBoxColumn.DataPropertyName = "Description";
            this.descriptionDataGridViewTextBoxColumn.HeaderText = "Description";
            this.descriptionDataGridViewTextBoxColumn.Name = "descriptionDataGridViewTextBoxColumn";
            this.descriptionDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // waxTypeDataGridViewTextBoxColumn
            // 
            this.waxTypeDataGridViewTextBoxColumn.DataPropertyName = "WaxType";
            this.waxTypeDataGridViewTextBoxColumn.HeaderText = "WaxType";
            this.waxTypeDataGridViewTextBoxColumn.Name = "waxTypeDataGridViewTextBoxColumn";
            this.waxTypeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // wickTypeDataGridViewTextBoxColumn
            // 
            this.wickTypeDataGridViewTextBoxColumn.DataPropertyName = "WickType";
            this.wickTypeDataGridViewTextBoxColumn.HeaderText = "WickType";
            this.wickTypeDataGridViewTextBoxColumn.Name = "wickTypeDataGridViewTextBoxColumn";
            this.wickTypeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // colorDataGridViewTextBoxColumn
            // 
            this.colorDataGridViewTextBoxColumn.DataPropertyName = "Color";
            this.colorDataGridViewTextBoxColumn.HeaderText = "Color";
            this.colorDataGridViewTextBoxColumn.Name = "colorDataGridViewTextBoxColumn";
            this.colorDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // scentDataGridViewTextBoxColumn
            // 
            this.scentDataGridViewTextBoxColumn.DataPropertyName = "Scent";
            this.scentDataGridViewTextBoxColumn.HeaderText = "Scent";
            this.scentDataGridViewTextBoxColumn.Name = "scentDataGridViewTextBoxColumn";
            this.scentDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // shimmeringDataGridViewCheckBoxColumn
            // 
            this.shimmeringDataGridViewCheckBoxColumn.DataPropertyName = "Shimmering";
            this.shimmeringDataGridViewCheckBoxColumn.HeaderText = "Shimmering";
            this.shimmeringDataGridViewCheckBoxColumn.Name = "shimmeringDataGridViewCheckBoxColumn";
            this.shimmeringDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // sizeDataGridViewTextBoxColumn
            // 
            this.sizeDataGridViewTextBoxColumn.DataPropertyName = "Size";
            this.sizeDataGridViewTextBoxColumn.HeaderText = "Size";
            this.sizeDataGridViewTextBoxColumn.Name = "sizeDataGridViewTextBoxColumn";
            this.sizeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // quantityDataGridViewTextBoxColumn
            // 
            this.quantityDataGridViewTextBoxColumn.DataPropertyName = "Quantity";
            this.quantityDataGridViewTextBoxColumn.HeaderText = "Quantity";
            this.quantityDataGridViewTextBoxColumn.Name = "quantityDataGridViewTextBoxColumn";
            this.quantityDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // lastOrderedQuantityDataGridViewTextBoxColumn
            // 
            this.lastOrderedQuantityDataGridViewTextBoxColumn.DataPropertyName = "LastOrderedQuantity";
            this.lastOrderedQuantityDataGridViewTextBoxColumn.HeaderText = "LastOrderedQuantity";
            this.lastOrderedQuantityDataGridViewTextBoxColumn.Name = "lastOrderedQuantityDataGridViewTextBoxColumn";
            this.lastOrderedQuantityDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // priceDataGridViewTextBoxColumn
            // 
            this.priceDataGridViewTextBoxColumn.DataPropertyName = "Price";
            this.priceDataGridViewTextBoxColumn.HeaderText = "Price";
            this.priceDataGridViewTextBoxColumn.Name = "priceDataGridViewTextBoxColumn";
            this.priceDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // lastOrderedDateDataGridViewTextBoxColumn
            // 
            this.lastOrderedDateDataGridViewTextBoxColumn.DataPropertyName = "LastOrderedDate";
            this.lastOrderedDateDataGridViewTextBoxColumn.HeaderText = "LastOrderedDate";
            this.lastOrderedDateDataGridViewTextBoxColumn.Name = "lastOrderedDateDataGridViewTextBoxColumn";
            this.lastOrderedDateDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // inventoryItemBindingSource
            // 
            this.inventoryItemBindingSource.DataSource = typeof(Milestone.InventoryItem);
            // 
            // InventoryTag
            // 
            this.InventoryTag.AutoSize = true;
            this.InventoryTag.Font = new System.Drawing.Font("Stencil", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InventoryTag.Location = new System.Drawing.Point(295, 63);
            this.InventoryTag.Name = "InventoryTag";
            this.InventoryTag.Size = new System.Drawing.Size(171, 34);
            this.InventoryTag.TabIndex = 2;
            this.InventoryTag.Text = "Inventory";
            // 
            // InventoryHeader
            // 
            this.InventoryHeader.AutoSize = true;
            this.InventoryHeader.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.InventoryHeader.Font = new System.Drawing.Font("Stencil", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InventoryHeader.Location = new System.Drawing.Point(76, 3);
            this.InventoryHeader.Name = "InventoryHeader";
            this.InventoryHeader.Size = new System.Drawing.Size(634, 57);
            this.InventoryHeader.TabIndex = 1;
            this.InventoryHeader.Text = "Inventory Management";
            // 
            // BackButton1
            // 
            this.BackButton1.Font = new System.Drawing.Font("Stencil", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BackButton1.Location = new System.Drawing.Point(10, 17);
            this.BackButton1.Name = "BackButton1";
            this.BackButton1.Size = new System.Drawing.Size(60, 43);
            this.BackButton1.TabIndex = 0;
            this.BackButton1.Text = "back";
            this.BackButton1.UseVisualStyleBackColor = true;
            this.BackButton1.Click += new System.EventHandler(this.BackButton1_Click);
            // 
            // MenuHeader
            // 
            this.MenuHeader.AutoSize = true;
            this.MenuHeader.Font = new System.Drawing.Font("Stencil", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MenuHeader.Location = new System.Drawing.Point(76, 0);
            this.MenuHeader.Name = "MenuHeader";
            this.MenuHeader.Size = new System.Drawing.Size(634, 57);
            this.MenuHeader.TabIndex = 3;
            this.MenuHeader.Text = "Inventory Management";
            // 
            // InventoryButton
            // 
            this.InventoryButton.Font = new System.Drawing.Font("Stencil", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InventoryButton.Location = new System.Drawing.Point(314, 221);
            this.InventoryButton.Name = "InventoryButton";
            this.InventoryButton.Size = new System.Drawing.Size(127, 36);
            this.InventoryButton.TabIndex = 2;
            this.InventoryButton.Text = "Inventory";
            this.InventoryButton.UseVisualStyleBackColor = true;
            this.InventoryButton.Click += new System.EventHandler(this.InventoryButton_Click);
            // 
            // LogoutButton
            // 
            this.LogoutButton.Font = new System.Drawing.Font("Stencil", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LogoutButton.Location = new System.Drawing.Point(314, 374);
            this.LogoutButton.Name = "LogoutButton";
            this.LogoutButton.Size = new System.Drawing.Size(127, 37);
            this.LogoutButton.TabIndex = 0;
            this.LogoutButton.Text = "logout";
            this.LogoutButton.UseVisualStyleBackColor = true;
            this.LogoutButton.Click += new System.EventHandler(this.LogoutButton_Click);
            // 
            // MenuTag
            // 
            this.MenuTag.AutoSize = true;
            this.MenuTag.Font = new System.Drawing.Font("Stencil", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MenuTag.Location = new System.Drawing.Point(318, 57);
            this.MenuTag.Name = "MenuTag";
            this.MenuTag.Size = new System.Drawing.Size(94, 34);
            this.MenuTag.TabIndex = 5;
            this.MenuTag.Text = "Menu";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Stencil", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(225, 342);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(309, 16);
            this.label1.TabIndex = 8;
            this.label1.Text = "HINT: The username and password is \"admin\"";
            // 
            // InventoryManagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.MenuPanel);
            this.Controls.Add(this.LoginButton);
            this.Controls.Add(this.PasswordTextBox);
            this.Controls.Add(this.UsernameTextBox);
            this.Controls.Add(this.LoginTag);
            this.Controls.Add(this.PasswordLabel);
            this.Controls.Add(this.UsernameLabel);
            this.Controls.Add(this.LoginHeader);
            this.Controls.Add(this.label1);
            this.Name = "InventoryManagement";
            this.Text = "INVENTORY MANAGEMENT - LOGIN";
            this.MenuPanel.ResumeLayout(false);
            this.MenuPanel.PerformLayout();
            this.InventoryPanel.ResumeLayout(false);
            this.InventoryPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inventoryItemBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LoginHeader;
        private System.Windows.Forms.Label UsernameLabel;
        private System.Windows.Forms.Label PasswordLabel;
        private System.Windows.Forms.Label LoginTag;
        private System.Windows.Forms.TextBox UsernameTextBox;
        private System.Windows.Forms.TextBox PasswordTextBox;
        private System.Windows.Forms.Button LoginButton;
        private System.Windows.Forms.Panel MenuPanel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button InventoryButton;
        private System.Windows.Forms.Button LogoutButton;
        private System.Windows.Forms.Label MenuHeader;
        private System.Windows.Forms.Panel InventoryPanel;
        private System.Windows.Forms.Button BackButton1;
        private System.Windows.Forms.Label InventoryHeader;
        private System.Windows.Forms.Label MenuTag;
        private System.Windows.Forms.Label InventoryTag;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn descriptionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn waxTypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn wickTypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn colorDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn scentDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn shimmeringDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sizeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn quantityDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lastOrderedQuantityDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn priceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lastOrderedDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource inventoryItemBindingSource;
    }
}

