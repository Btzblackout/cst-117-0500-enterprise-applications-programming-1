﻿namespace PlaygroundApp
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Username = new System.Windows.Forms.Label();
            this.Password = new System.Windows.Forms.Label();
            this.UserInput = new System.Windows.Forms.TextBox();
            this.PassInput = new System.Windows.Forms.TextBox();
            this.Submit = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.RememberInfo = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Username
            // 
            this.Username.AutoSize = true;
            this.Username.Font = new System.Drawing.Font("Showcard Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.Username.ForeColor = System.Drawing.Color.Coral;
            this.Username.Location = new System.Drawing.Point(116, 161);
            this.Username.Name = "Username";
            this.Username.Size = new System.Drawing.Size(115, 23);
            this.Username.TabIndex = 0;
            this.Username.Text = "Username";
            // 
            // Password
            // 
            this.Password.AutoSize = true;
            this.Password.Font = new System.Drawing.Font("Showcard Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.Password.ForeColor = System.Drawing.Color.Coral;
            this.Password.Location = new System.Drawing.Point(116, 213);
            this.Password.Name = "Password";
            this.Password.Size = new System.Drawing.Size(124, 23);
            this.Password.TabIndex = 1;
            this.Password.Text = "Password";
            // 
            // UserInput
            // 
            this.UserInput.Location = new System.Drawing.Point(291, 162);
            this.UserInput.Name = "UserInput";
            this.UserInput.Size = new System.Drawing.Size(184, 23);
            this.UserInput.TabIndex = 2;
            // 
            // PassInput
            // 
            this.PassInput.Location = new System.Drawing.Point(291, 213);
            this.PassInput.Name = "PassInput";
            this.PassInput.PasswordChar = '*';
            this.PassInput.Size = new System.Drawing.Size(184, 23);
            this.PassInput.TabIndex = 3;
            // 
            // Submit
            // 
            this.Submit.BackColor = System.Drawing.SystemColors.Control;
            this.Submit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Submit.Font = new System.Drawing.Font("Showcard Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.Submit.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.Submit.Location = new System.Drawing.Point(253, 321);
            this.Submit.Name = "Submit";
            this.Submit.Size = new System.Drawing.Size(104, 27);
            this.Submit.TabIndex = 4;
            this.Submit.Text = "Login";
            this.Submit.UseVisualStyleBackColor = false;
            // 
            // button1
            // 
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.Font = new System.Drawing.Font("Showcard Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button1.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.button1.Location = new System.Drawing.Point(253, 375);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(104, 27);
            this.button1.TabIndex = 5;
            this.button1.Text = "Register";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // RememberInfo
            // 
            this.RememberInfo.AutoSize = true;
            this.RememberInfo.Font = new System.Drawing.Font("Showcard Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.RememberInfo.ForeColor = System.Drawing.Color.Coral;
            this.RememberInfo.Location = new System.Drawing.Point(291, 266);
            this.RememberInfo.Name = "RememberInfo";
            this.RememberInfo.Size = new System.Drawing.Size(113, 19);
            this.RememberInfo.TabIndex = 6;
            this.RememberInfo.Text = "Remember Me";
            this.RememberInfo.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.DarkCyan;
            this.label1.Font = new System.Drawing.Font("Showcard Gothic", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.ForeColor = System.Drawing.Color.Coral;
            this.label1.Location = new System.Drawing.Point(139, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(336, 60);
            this.label1.TabIndex = 7;
            this.label1.Text = "My Account";
            // 
            // Form1
            // 
            this.BackColor = System.Drawing.Color.DarkCyan;
            this.ClientSize = new System.Drawing.Size(623, 449);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.RememberInfo);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Submit);
            this.Controls.Add(this.PassInput);
            this.Controls.Add(this.UserInput);
            this.Controls.Add(this.Password);
            this.Controls.Add(this.Username);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.Name = "Form1";
            this.Text = "My Account - LOGIN";
            this.ResumeLayout(false);
            this.PerformLayout();

        }



        #endregion

        private System.Windows.Forms.Label Username;
        private System.Windows.Forms.Label Password;
        private System.Windows.Forms.TextBox UserInput;
        private System.Windows.Forms.TextBox PassInput;
        private System.Windows.Forms.Button Submit;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox RememberInfo;
        private System.Windows.Forms.Label label1;
    }
}

